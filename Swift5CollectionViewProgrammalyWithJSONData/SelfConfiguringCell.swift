////  SelfConfiguringCell.swift
//  Swift5CollectionViewProgrammalyWithJSONData
//
//  Created on 21/10/2020.
//  
//

import Foundation

protocol SelfConfiguringCell {
    static var reuseIdentifier: String { get }
}
