////  ContactCell.swift
//  Swift5CollectionViewProgrammalyWithJSONData
//
//  Created on 21/10/2020.
//  
//

import UIKit
import Kingfisher
import TinyConstraints

class ContactCell: UICollectionView, SelfConfiguringCell {
    
    static let reuseIdentifier: String = "ContactCell"
    
    private let imageApiBaseUrl = "https://api.adorable.io/avatars/285/"
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private var imageView: UIImageView = {
        let i = UIImageView()
        i.layer.contents = 22
        i.clipsToBounds = true
        return i
    }()
    
    private var activityIndicatorView: UIActivityIndicatorView = {
        let a = UIActivityIndicatorView()
        a.startAnimating()
        return a
    }()
    
    private var nameLabel: UILabel = {
        let l = UILabel()
        l.font = .systemFont(ofSize: 22)
        l.textColor = UIColor.init(white: 0.3, alpha: 0.8)
        return l
    }()
    
    private var emailLabel: UILabel = {
        let l = UILabel()
        l.font = .boldSystemFont(ofSize: 12)
        l.textColor = UIColor.init(white: 0.3, alpha: 0.4)
        return l
    }()
    
    private func setupView() {
    
        backgroundColor = .secondarySystemBackground
        layer.cornerRadius = 8
        clipsToBounds = true
        
        addSubwiews(imageView, nameLabel, emailLabel, activityIndicatorView)
        
        imageView.edgesToSuperview(excluding: .trailing, insets: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8))
        imageView.size(CGSize(width: 44, height: 44))
        
        activityIndicatorView.center(in: imageView)
        
        nameLabel.leadingToTrailing(of: imageView,offset: 8)
        nameLabel.trailingToSuperview(offset: 8)
        nameLabel.centerY(to: imageView, offset: -10)
        
        emailLabel.leadingToTrailing(of: imageView,offset: 8)
        emailLabel.trailingToSuperview(offset: 8)
        emailLabel.centerY(to: imageView, offset: -10)
    }
    
    func configure ( with contact: Contact) {
        let imageUrl = "\(imageApiBaseUrl)\(contact.email)"
        if let url = URL(string: imageUrl) {
            let resource = ImageResource(downloadURL: url)
            self.imageView.kf.setImage(with: resource, placeholder: nil, options: nil, progressBlock: nil) { (result: Result<RetrieveImageResult, KingfisherError>) in
                switch result {
                    case .success(_) :
                        self.activityIndicatorView.stopAnimating()
                    case .failure(let err) :
                    print(err.localizedDescription)
                }
            }
        }
        
        nameLabel.text = contact.name
        emailLabel.text = contact.email
    }
}

extension UIView {
    func  addSubwiews(_ views: UIView ...) {
        for view in views {
            addSubview(view)
        }
    }
}



