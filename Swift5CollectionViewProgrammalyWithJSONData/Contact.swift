////  Contact.swift
//  Swift5CollectionViewProgrammalyWithJSONData
//
//  Created on 21/10/2020.
//  
//

import Foundation

struct Contact: Codable, Hashable {
    
    let id: Int
    let name: String
    let username: String
    let email: String
    let address: Address
    
    struct Address: Codable, Hashable {
        
        let street: String
        let suite: String
        let city: String
        let zipcode: String
        let geo: Geo
        
        struct Geo: Codable, Hashable {
            let lat: String
            let lng: String
        }
    }
    
    let phone: String
    let website: String
    let company: Company
    
    struct Company: Codable, Hashable {
        let name: String
        let catchPhrase: String
        let bs: String
    }
}


